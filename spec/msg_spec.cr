require "spec"
require "../src/msg/core.cr"
require "../src/msg.cr"

describe "WikiLinks" do
  describe "Parse" do
    it "must parse a plain wikilink correctly and back" do
      wikilink = "This is a plain WikiLink"
      l = WikiLink.new wikilink
      l.name.should eq(wikilink)
      l.hash.should be_nil
      l.link.should eq(wikilink)
      reg = l.to_regular_link
      reg.should eq("[This is a plain WikiLink](/this-is-a-plain-wikilink.html)")
    end
    it "must parse a wikilink with title correctly and back" do
      wikilink = "This is a plain WikiLink|Alternative title"
      l = WikiLink.new wikilink
      l.name.should eq("Alternative title")
      l.hash.should be_nil
      l.link.should eq("This is a plain WikiLink")
      reg = l.to_regular_link
      reg.should eq("[Alternative title](/this-is-a-plain-wikilink.html)")
    end
    it "must parse a wikilink with title and hash correctly and back" do
      wikilink = "This is a plain WikiLink#Index|Alternative title"
      l = WikiLink.new wikilink
      l.name.should eq("Alternative title")
      l.hash.should eq("Index")
      l.link.should eq("This is a plain WikiLink")
      reg = l.to_regular_link
      reg.should eq("[Alternative title](/this-is-a-plain-wikilink.html#Index)")
    end
    it "must parse a wikilink with just hash correctly and back" do
      wikilink = "This is a plain WikiLink#Index"
      l = WikiLink.new wikilink
      l.name.should eq("This is a plain WikiLink")
      l.hash.should eq("Index")
      l.link.should eq("This is a plain WikiLink")
      reg = l.to_regular_link
      reg.should eq("[This is a plain WikiLink](/this-is-a-plain-wikilink.html#Index)")
    end
  end
  describe "Files" do
    it "must get the correct title for a file" do
      md = MarkdownFile.new "#{__DIR__}/assets/Test file.md"
      md.title.should eq("Test file")
    end
  end
end
