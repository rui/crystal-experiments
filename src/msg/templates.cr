require "ecr"

class Page
  def initialize(@source : SourceFile)
  end

  ECR.def_to_s "#{__DIR__}/templates/page.ecr"
end
