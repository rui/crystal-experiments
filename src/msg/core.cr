require "cmark"

def get_markdown_files(path : String) : Array(String)
  files = Dir.glob("#{path}/**/*")
  files.select { |file| file.ends_with?(".md") }
end

def get_frontmatter(markdown : String) : String | Nil
  matches = markdown.match(/\A---(.|\n)*?---/)
  if matches
    return matches[0]
  else
    return nil
  end
end

def sanitize_link(link : String) : String
  return link.gsub(" ", "-")
    .gsub("'", "")
    .gsub("(", "")
    .gsub(")", "")
    .downcase
end

class WikiLink
  getter name : String
  getter hash : String?
  getter link : String
  getter original

  def initialize(@original : String)
    if !original.includes?("|") && !original.includes?("#")
      @name = @original
      @hash = nil
      @link = @original
    elsif original.includes?("|") && !original.includes?("#")
      parts = original.split("|")
      @name = parts[1]
      @hash = nil
      @link = parts[0]
    elsif original.includes?("|") && original.includes?("#")
      parts = original.split("|")
      link_part = parts[0].split("#")
      @name = parts[1]
      @hash = link_part[1]
      @link = link_part[0]
    elsif !original.includes?("|") && original.includes?("#")
      parts = original.split("#")
      @name = parts[0]
      @hash = parts[1]
      @link = parts[0]
    else
      @name = ""
      @hash = nil
      @link = ""
    end
  end

  def to_regular_link : String
    sanitized_link = sanitize_link(link)
    if @hash
      return "[#{@name}](/#{sanitized_link}.html##{@hash})"
    else
      return "[#{@name}](/#{sanitized_link}.html)"
    end
  end
end

def get_backlinks(to : SourceFile, _in : Array(SourceFile)) : Array(SourceFile)
  _in.select { |source_file|
    source_file.forward_links.find { |link| link.name == to.title } != nil
  }
end

def make_toc(source : String, minimum_level : Int32 = 2) : String
  options = Cmark::Option.flags(Footnotes, Nobreaks, ValidateUTF8) # deafult is Option::None
  extensions = Cmark::Extension.flags(Table, Tasklist, Autolink)   # default is Extension::None

  node = Cmark.parse_document(source, options, extensions)

  iterator = Cmark::EventIterator.new(node)

  markdown = [] of String
  iterator.each do |event|
    node = event.node
    if event.enter?
      # puts "entering node"
    end
    if event.modifiable? && node.type.heading? && node.heading_level >= minimum_level
      markdown << "\t"*(node.heading_level - minimum_level) + "- [#{node.render_plaintext.chomp}](##{sanitize_link(node.render_plaintext.chomp)})"
    end
  end
  return markdown.join("\n")
end
