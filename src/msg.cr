require "cmark"
require "./msg/core.cr"
require "./msg/templates.cr"
require "option_parser"
require "progress_bar.cr/src/progress_bar.cr"
require "colorize"
require "holst"

class SourceFile
  property frontmatter : String?
  getter forward_links : Array(WikiLink)
  getter title : String
  getter url : String
  property backlinks : Array(SourceFile)
  getter toc : String

  def initialize(@contents : String, @title : String)
    @frontmatter = get_frontmatter(@contents)
    # if the frontmatter is not nil, remove it from the contents
    if @frontmatter
      @contents = @contents.gsub(@frontmatter.to_s, "")
    end
    @forward_links = parse_wikilinks
    # The title is the name of the file sans extension
    @url = "#{sanitize_link(@title)}.html"
    @backlinks = [] of SourceFile
    @toc = ""
  end

  def parse_wikilinks : Array(WikiLink)
    matches = @contents.scan(/\[\[(.*?)]]/)
    if matches
      return matches.map { |match| WikiLink.new match[1] }
    end
    return [] of WikiLink
  end

  def replace_wikilinks
    @forward_links.each { |link|
      @contents = @contents.gsub("[[#{link.original}]]", link.to_regular_link)
    }
  end

  def to_html : String
    replace_wikilinks

    # renderer options
    options = Cmark::Option.flags(Footnotes, Nobreaks, ValidateUTF8) # deafult is Option::None
    extensions = Cmark::Extension.flags(Table, Tasklist)             # default is Extension::None

    # render TOC
    _toc = make_toc(@contents)
    @toc = Cmark.document_to_html(_toc, options, extensions)
    # render the markdown

    return Cmark.document_to_html(@contents, options, extensions)
  end
end

class MarkdownFile < SourceFile
  def initialize(file_path : String)
    contents = File.read(file_path)
    path = Path.posix(file_path)
    title = path.basename.chomp(path.extension)
    super(contents, title)
  end
end

class JupyterFile < SourceFile
  def initialize(file_path : String, output : String)
    path = Path.posix(file_path)
    title = path.basename.chomp(path.extension)
    image_dest = Path.posix(output) / "images"
    notebook = Holst::JupyterFile.new(file_path, image_prefix: sanitize_link(title), image_dest: image_dest.to_s)
    contents = notebook.to_markdown
    notebook.export_images
    super(contents, title)
  end
end

def create_pair_path(markdown_path : String) : String
  path = Path.posix(markdown_path)
  common = Path.posix(path.dirname)
  name = path.basename.chomp(path.extension)
  notebook = common / "#{name}.ipynb"
  return notebook.to_s
end

# check if a markdown file has a jupyter pair
def has_pair?(file_path : String) : Bool
  notebook = create_pair_path(file_path)
  return File.exists?(notebook)
end

def build(input : String, output : String)
  input_path = Path.posix(input).expand(home: true)
  md = get_markdown_files(input_path.to_s)
  sources = md.map { |file|
    if has_pair?(file)
      JupyterFile.new(create_pair_path(file), output).as(SourceFile)
    else
      MarkdownFile.new(file).as(SourceFile)
    end
  }
  pb = ProgressBar.new(ticks: md.size, show_percentage: true)
  pb.init
  pb.message("[STATUS]".colorize(:green).to_s + " Converting sources ...")
  sources.each { |source|
    source.backlinks = get_backlinks(source, sources)
    html = Page.new(source).to_s
    # puts("Saving to #{Path.posix(output).expand(home: true).join(sanitize_link(source.title) + ".html")}")
    destination = Path.posix(output).expand(home: true).join(sanitize_link(source.title) + ".html")
    File.write(destination.to_s, html)
    pb.tick
  }
  puts("[STATUS]".colorize(:green).to_s + " Done.")
end

input : String? = nil
output : String? = nil

OptionParser.parse do |parser|
  parser.banner = "Usage: msg [arguments]"
  parser.on("-i INPUT", "--input=INPUT", "Where the input files are") { |name| input = name }
  parser.on("-o OUTPUT", "--output=OUTPUT", "Where the output files are") { |name| output = name }
  parser.on("-h", "--help", "Show this help") { puts parser }
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit(1)
  end
end

if input && output
  build(input.to_s, output.to_s)
end
